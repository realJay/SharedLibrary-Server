package pers.hl.library.utils;

import org.springframework.util.CollectionUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.condition.PatternsRequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;

public final class MyUtils {

    private static List<String> urls = new ArrayList<>();

    public static List<String> getAllUrl() {
        if (!CollectionUtils.isEmpty(urls)) {
            LogUtils.i("使用缓存url:" + urls);
            return urls;
        }
        // 获取springmvc处理器映射器组件对象 RequestMappingHandlerMapping无法直接注入
        RequestMappingHandlerMapping mapping = SpringBeanTool.getBean(RequestMappingHandlerMapping.class);
        //获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();

//        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> m : map.entrySet()) {
            RequestMappingInfo info = m.getKey();
            HandlerMethod method = m.getValue();
            // 获取url
            PatternsRequestCondition p = info.getPatternsCondition();
            urls.addAll(p.getPatterns());
            urls.forEach(new Consumer<String>() {
                @Override
                public void accept(String s) {
                }
            });
            // 反射获取url对应类名和方法名
//            urlMap.put("className", method.getMethod().getDeclaringClass().getName()); // 类名
//            urlMap.put("method", method.getMethod().getName()); // 方法名

            // 获取请求类型
//            RequestMethodsRequestCondition methodsRequestCondition = info.getMethodsCondition();
//            for (RequestMethod requestMethod : methodsRequestCondition.getMethods()) {
//                urlMap.put("type", requestMethod.toString());
//            }
//            list.add(urlMap);
        }
        System.out.println("urls=" + urls);
        return urls;
    }

    public static void getAllMappedUrl() {

    }

    /**
     * 获取纯净(没有符号)的uuid
     */
    public static String getPureUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * 验证id是否为空(有效)
     *
     * @param id
     * @return
     */
    public static boolean idValid(Object id) {
        if (id instanceof Integer) {
            return id != null && (Integer) id > 0;
        } else if (id instanceof Long) {
            return id != null && (Long) id > 0;
        }
        return false;
    }

    public static int tryParseInt(String value, int defaultValue) {
        int result;
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return defaultValue;
        }
        return result;
    }
}
