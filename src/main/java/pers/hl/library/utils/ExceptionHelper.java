package pers.hl.library.utils;

import pers.hl.library.common.Const;
import pers.hl.library.common.CustomException;

/**
 * 异常抛出帮助类，封装业务常见异常
 */
public final class ExceptionHelper {

    public static void throw400() {
        throw400("参数错误");
    }

    public static void throw400(String msg) {
        throwCustomException(Const.HttpStatusCode.HttpStatus_400, msg);
    }

    public static void throw403() {
        throw403("没有访问该资源的权限");
    }

    public static void throw403(String msg) {
        throwCustomException(Const.HttpStatusCode.HttpStatus_403, msg);
    }

    public static void throw404() {
        throw404("未找到资源");
    }

    public static void throw404(String msg) {
        throwCustomException(Const.HttpStatusCode.HttpStatus_404, msg);
    }

    public static void throw500() {
        throw500("服务端错误");
    }

    public static void throw500(String msg) {
        throwCustomException(Const.HttpStatusCode.HttpStatus_500, msg);
    }

    public static void throwCustomException(String code, String msg) {
        throw new CustomException(code, msg);
    }
}
