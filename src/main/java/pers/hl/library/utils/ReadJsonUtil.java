package pers.hl.library.utils;

import java.io.*;

public class ReadJsonUtil {

    public static String readFromFile(String fileName) {

        try {
            InputStream inputStream;
//            ClassPathResource classPathResource = new ClassPathResource("static/assets/" + fileName);
//            inputStream = classPathResource.getInputStream();
            File file = new File("src/main/resources/static/assets/" + fileName);
            inputStream = new FileInputStream(file);
            StringBuilder stringBuilder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "utf-8"));
            String line;
            while((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
            reader.close();
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
