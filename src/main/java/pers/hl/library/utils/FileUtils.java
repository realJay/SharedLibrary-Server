package pers.hl.library.utils;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.FileSystemException;

public final class FileUtils {

    /**
     * 写入文件
     * @param origin 原始文件
     * @param dest 目标文件
     */
    public static void writeFile(MultipartFile origin, File dest) throws Exception{
            // 检测是否存在目录
            if (!dest.getParentFile().exists()) {
                // 新建文件夹
                if (dest.getParentFile().mkdirs()) {
                    // 文件写入
                    origin.transferTo(dest);
                    LogUtils.i("写入的文件路径：" + dest.getAbsolutePath());
                } else {
                    throw new FileSystemException("创建文件失败");
                }
            } else {
                // 文件写入
                origin.transferTo(dest);
                LogUtils.i("写入的文件路径：" + dest.getAbsolutePath());
            }
    }

    /**
     * 根据文件路径读取文件并输出流
     * @param request 请求
     * @param response 响应
     * @param realPath 文件真实路径
     */
    public static void readFile(HttpServletRequest request, HttpServletResponse response, String realPath) throws IOException {
        //获取输入流对象（用于读文件）
        FileInputStream fis = new FileInputStream(new File(realPath));
        //获取文件后缀（如.txt）
        String extendFileName = realPath.substring(realPath.lastIndexOf('.'));
        //动态设置响应类型，根据前台传递文件类型设置响应类型
        response.setContentType(request.getSession().getServletContext().getMimeType(extendFileName));
        //设置响应头,attachment表示以附件的形式下载，inline表示在线打开
//        response.setHeader("content-disposition", "attachment;filePath=" + URLEncoder.encode(realPath, "UTF-8"));
        response.setHeader("content-disposition", "inline;filePath=" + URLEncoder.encode(realPath, "UTF-8"));
        //获取输出流对象（用于写文件）
        ServletOutputStream os = response.getOutputStream();
        //下载文件,使用spring框架中的FileCopyUtils工具
        FileCopyUtils.copy(fis, os);
    }

    /**
     * 删除文件
     * @param file 待删除的文件
     */
    public static boolean deleteFile(final File file) {
        return file != null && (!file.exists() || file.isFile() && file.delete());
    }
}
