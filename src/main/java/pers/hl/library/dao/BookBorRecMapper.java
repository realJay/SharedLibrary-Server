package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.BookBorRec;
import pers.hl.library.po.BookBorRecExample;

import java.util.List;

@Mapper
public interface BookBorRecMapper extends IMapper<BookBorRec, BookBorRecExample> {
    int countByExample(BookBorRecExample example);

    int deleteByExample(BookBorRecExample example);

    int deleteByPrimaryKey(Integer bbrId);

    int insert(BookBorRec record);

    int insertSelective(BookBorRec record);

    List<BookBorRec> selectByExample(BookBorRecExample example);

    BookBorRec selectByPrimaryKey(Integer bbrId);

    int updateByExampleSelective(@Param("record") BookBorRec record, @Param("example") BookBorRecExample example);

    int updateByExample(@Param("record") BookBorRec record, @Param("example") BookBorRecExample example);

    int updateByPrimaryKeySelective(BookBorRec record);

    int updateByPrimaryKey(BookBorRec record);

    List<BookBorRec> myBorrows(Integer borrowerId);

    List<BookBorRec> myBorrowed(Integer borrowerId);

    List<BookBorRec> getExpiringRecords(Integer userId);
}