package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.User;
import pers.hl.library.po.UserExample;

import java.util.List;

@Mapper
public interface UserMapper extends IMapper<User, UserExample> {

    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer userId);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    List<User> getDataList();

    User getDataByPrimaryId(String id);

    int exist(String account);

    User validate(String account, String password);

    int updatePassword(@Param("record") User record);

    Integer getSharingCount(Integer userId);

    Integer getFollowCount(Integer userId);

    Integer getFansCount(Integer userId);
}