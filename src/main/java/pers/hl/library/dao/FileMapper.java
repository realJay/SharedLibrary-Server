package pers.hl.library.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import pers.hl.library.common.base.IMapper;
import pers.hl.library.po.FileEntity;
import pers.hl.library.po.FileExample;

import java.util.List;

@Mapper
public interface FileMapper extends IMapper<FileEntity, FileExample> {
    int countByExample(FileExample example);

    int deleteByExample(FileExample example);

    int deleteByPrimaryKey(Integer fileId);

    int insert(FileEntity record);

    int insertSelective(FileEntity record);

    List<FileEntity> selectByExample(FileExample example);

    FileEntity selectByPrimaryKey(Integer fileId);

    int updateByExampleSelective(@Param("record") FileEntity record, @Param("example") FileExample example);

    int updateByExample(@Param("record") FileEntity record, @Param("example") FileExample example);

    int updateByPrimaryKeySelective(FileEntity record);

    int updateByPrimaryKey(FileEntity record);

    int batchDelete(List<Integer> list);
}