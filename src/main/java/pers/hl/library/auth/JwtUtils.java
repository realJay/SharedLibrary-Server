package pers.hl.library.auth;

import io.jsonwebtoken.*;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

/**
 * JSON Web Token 工具类
 *
 * 使用JWT存在弊端，比如用户修改密码，注销登录后，原来的token只要没过期就还能用。
 * 只能暂时在客户端做操作，清除本地token重新登录了
 */
public class JwtUtils {

    /**
     * key（按照签名算法的字节长度设置key）
     */
    private static final String SECRET_KEY = "0123456789_0123456789_0123456789";
    /**
     * 过期时间（毫秒单位）,测试的过程中可以写长一点，主要是懒得老去登录
     */
    public final static long TOKEN_EXPIRE_MILLIS = 596 * 60  * 60 * 1000; // 24小时 24 * 60 * 60 * 1000

    /**
     * 创建token
     *
     * @param claimMap
     * @return
     */
    public static String createToken(Map<String, Object> claimMap) {
        long currentTimeMillis = System.currentTimeMillis();
        return Jwts.builder()
                .setClaims(claimMap) // 设置claims，以参数创建一个新Claims对象，直接赋值
                .setId(UUID.randomUUID().toString())
//                .setAudience("") // 接受者
                .setIssuedAt(new Date(currentTimeMillis))    // 设置签发时间
                .setExpiration(new Date(currentTimeMillis + TOKEN_EXPIRE_MILLIS))   // 设置过期时间,setClaims必须在此之前，才能正确设置
                .signWith(SignatureAlgorithm.HS256, generateKey())
                .compact();
    }

    /**
     * 验证token
     *
     * @param token
     * @return 0 验证成功，1、2、3、4、5 验证失败
     */
    public static int verifyToken(String token) {
        try {
            Jwts.parser().setSigningKey(generateKey()).parseClaimsJws(token);
            return 0;
        } catch (ExpiredJwtException e) {
            return 1;
        } catch (UnsupportedJwtException e) {
            return 2;
        } catch (MalformedJwtException e) {
            e.printStackTrace();
            return 3;
        } catch (SignatureException e) {
            return 4;
        } catch (IllegalArgumentException e) {
            return 5;
        }
    }

    public static boolean verifyPass(String token) {
        return verifyToken(token) == 0;
    }

    /**
     * 解析token
     *
     * @param token
     * @return
     */
    public static Claims parseToken(String token) throws Exception {
        try {
            return Jwts.parser()  // 得到DefaultJwtParser
                    .setSigningKey(generateKey()) // 设置签名密钥
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            throw new AuthException(String.format("token过期，请重新登录（%s）：%s", e.getClass().getSimpleName(), e.getMessage()));
        } catch (UnsupportedJwtException e) {
            throw new AuthException(String.format("不支持的JWT异常（%s）：%s", e.getClass().getSimpleName(), e.getMessage()));
        } catch (MalformedJwtException e) {
            throw new AuthException(String.format("token格式错误（%s）：%s", e.getClass().getSimpleName(), e.getMessage()));
        } catch (SignatureException e) {
            throw new AuthException(String.format("token签名错误（%s）：%s", e.getClass().getSimpleName(), e.getMessage()));
        } catch (IllegalArgumentException e) {
            throw new AuthException(String.format("非法参数错误（%s）：%s", e.getClass().getSimpleName(), e.getMessage()));
        }
    }

    /**
     * 生成安全密钥
     *
     * @return
     */
    public static Key generateKey() {
        return new SecretKeySpec(SECRET_KEY.getBytes(), SignatureAlgorithm.HS256.getJcaName());
    }

    /**
     * 判断token是否过期
     */
    public static boolean isTokenExpired(String token) throws Exception {
        Claims body = parseToken(token);
        Date expiration = body.getExpiration();
        return expiration.before(new Date());
    }
}
