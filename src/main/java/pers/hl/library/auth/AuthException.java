package pers.hl.library.auth;

import pers.hl.library.common.Const;

public class AuthException extends RuntimeException {

    private static final long serialVersionUID = 8109469326798389194L;

    protected String errorCode = Const.HttpStatusCode.HttpStatus_401;

    private static final String DEFAULT_ERROR = "认证失败, 请重新登录";

    /**
     * 默认
     */
    public AuthException() {
        super(DEFAULT_ERROR);
    }

    public AuthException(String errorMessage) {
        super(errorMessage);
    }

}
