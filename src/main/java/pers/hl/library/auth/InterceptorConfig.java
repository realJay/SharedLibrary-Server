package pers.hl.library.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @Configuration 注解的类要
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Resource
    TokenInterceptor tokenInterceptor;
    @Resource
    RoleInterceptor roleInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenInterceptor)
                .addPathPatterns("/**"); // "/**"表示拦截所有请求,这里我只想拦截我部署的url
        registry.addInterceptor(roleInterceptor)
                .addPathPatterns("/**");
    }




}
