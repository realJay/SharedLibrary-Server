package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.FollowRecMapper;
import pers.hl.library.po.FollowRec;
import pers.hl.library.po.FollowRecExample;

import java.util.List;

@Service
public class FollowService extends BaseService<FollowRec, FollowRecExample> {

    @Autowired
    FollowRecMapper followRecMapper;

    @Autowired
    public FollowService(FollowRecMapper followRecMapper) {
        init(followRecMapper);
    }

    public boolean isFollow(int followerId, int userId) {
        mExample = createExample();
        mExample.createCriteria().andFrFollowerIdEqualTo(followerId).andFrUserIdEqualTo(userId);
        List<FollowRec> list = dao.selectByExample(mExample);
        return !ObjectUtils.isEmpty(list);
    }

    public boolean unFollow(int followerId, int userId) {
        mExample = createExample();
        mExample.createCriteria().andFrFollowerIdEqualTo(followerId).andFrUserIdEqualTo(userId);
        return dao.deleteByExample(mExample) > 0;
    }
}
