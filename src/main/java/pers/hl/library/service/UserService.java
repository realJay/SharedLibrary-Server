package pers.hl.library.service;

import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.FollowRecMapper;
import pers.hl.library.dao.UserMapper;
import pers.hl.library.po.User;
import pers.hl.library.po.UserExample;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService extends BaseService<User, UserExample> {

    @Resource
    UserMapper userMapper;
    @Resource
    FollowRecMapper followRecMapper;

    public UserService(UserMapper userMapper) {
        init(userMapper);
    }

    public boolean exist(String account) {
        return userMapper.exist(account) > 0;
    }

    public User getUser(String account, String password) {
        return userMapper.validate(account, password);
    }

    @Override
    public boolean update(User user) {
        // 默认注册普通用户
        user.setUserRole(User.ROLE_COMMON);
        return userMapper.updateByPrimaryKeySelective(user) > 0;
    }

    public User getData() {
        return null;
    }

    @Override
    public User getDataByPrimaryId(Integer id) {
        return (User) userMapper.getDataByPrimaryId(String.valueOf(id));
    }

    @Override
    public List<User> getDataList(String key, Integer otherId) {
        switch (key) {
            case "follows":
                // 用户关注列表
                return followRecMapper.userFollows(otherId);
            case "fans":
                // 用户粉丝列表
                return followRecMapper.userFans(otherId);
            default:
                return userMapper.getDataList();
        }
    }

    @Override
    public boolean addData(User user) {
        return userMapper.insertSelective(user) > 0;
    }

    @Override
    public boolean batchAdd(List<User> list) {
        return false;
    }

    @Override
    public boolean deleteData(Integer id) {
        return false;
    }

    public boolean modifyPwd(User user) {
        return userMapper.updatePassword(user) > 0;
    }

    /**
     * 分享数量
     * @param userId
     * @return
     */
    public Integer getSharingCount(Integer userId) {
        return userMapper.getSharingCount(userId);
    }

    /**
     * -- 我的关注数量，我作为粉丝
     * select count(1) from follow_rec where fr_follower_id = 1;
     */
    public Integer getFollowCount(Integer userId) {
        return userMapper.getFollowCount(userId);
    }

    /**
     * -- 我的粉丝数量,我作为被关注者
     * select count(1) from follow_rec where fr_user_id = 1;
     * @param userId
     * @return
     */
    public Integer getFansCount(Integer userId) {
        return userMapper.getFansCount(userId);
    }
}
