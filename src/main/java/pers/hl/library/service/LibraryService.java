package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.LibraryMapper;
import pers.hl.library.po.Library;
import pers.hl.library.po.LibraryExample;

import java.util.List;
import java.util.Map;

@Service
public class LibraryService extends BaseService<Library, LibraryExample> {

    @Autowired
    LibraryMapper libraryMapper;

    public LibraryService(LibraryMapper libraryMapper) {
        init(libraryMapper);
    }

    @Override
    public Library getDataByPrimaryId(Integer id) {
        return libraryMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean addData(Library data) {
        return libraryMapper.insertSelective(data) > 0;
    }

    @Override
    public boolean batchAdd(List<Library> list) {
        return false;
    }

    @Override
    public boolean deleteData(Integer id) {
        return libraryMapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean batchDelete(List<Integer> ids) {
        return libraryMapper.batchDelete(ids) > 0;
    }

    @Override
    public boolean update(Library data) {
        return libraryMapper.updateByPrimaryKeySelective(data) > 0;
    }

    public List<Library> getNearbyLibs(Map<String, Double> map) {
        return libraryMapper.getNearbyLibs(map);
    }

    @Override
    public List<Library> getDataList2(String key, String value) {
        switch (key) {
            case "search":
                return searchLibraries(value);
        }
        return super.getDataList2(key, value);
    }

    private List<Library> searchLibraries(String keyword) {
        String condition = "%" + keyword + "%";
        mExample = createExample();
        LibraryExample.Criteria c1 = mExample.createCriteria();
        // 全匹配
        c1.andLibNameLike(condition);
        LibraryExample.Criteria c2 = mExample.createCriteria();
        c2.andLibLocationLike(condition);
        mExample.or(c2);
        return dao.selectByExample(mExample);
    }
}
