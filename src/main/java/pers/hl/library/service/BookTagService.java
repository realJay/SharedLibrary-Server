package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.BookTagMapper;
import pers.hl.library.po.BookTag;
import pers.hl.library.po.BookTagExample;

import java.util.List;

@Service
public class BookTagService extends BaseService<BookTag, BookTagExample> {

    @Autowired
    BookTagMapper bookTagMapper;

    public BookTagService(BookTagMapper bookTagMapper) {
        init(bookTagMapper);
    }

    @Override
    public List<BookTag> getDataList(Integer id) {
        mExample = createExample();
        return bookTagMapper.selectByExample(mExample);
    }

    public List<BookTag> getTagByName(String name) {
        mExample = createExample();
        mExample.createCriteria().andNameEqualTo(name);
        return bookTagMapper.selectByExample(mExample);
    }
}
