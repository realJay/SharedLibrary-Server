package pers.hl.library.service;

import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.BookBorRecMapper;
import pers.hl.library.dao.BookMapper;
import pers.hl.library.dao.BookTagMapper;
import pers.hl.library.dao.CollectMapper;
import pers.hl.library.po.Book;
import pers.hl.library.po.BookBorRec;
import pers.hl.library.po.BookExample;
import pers.hl.library.utils.ExceptionHelper;
import pers.hl.library.utils.MyUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BookService extends BaseService<Book, BookExample> {

    @Resource
    BookMapper bookMapper;
    @Resource
    BookTagMapper bookTagMapper;
    @Resource
    BookBorRecMapper bookBorRecMapper;
    @Resource
    CollectMapper collectMapper;

    public BookService(BookMapper bookMapper) {
        init(bookMapper);
    }

    @Override
    public Book getDataByPrimaryId(Integer id) {
        return bookMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean addData(Book data) {
        return bookMapper.insertSelective(data) > 0;
    }

    @Override
    public boolean batchAdd(List<Book> list) {
        return false;
    }

    @Override
    public boolean deleteData(Integer id) {
        return bookMapper.deleteByPrimaryKey(id) > 0;
    }

    /**
     * 书籍信息修改
     * 修改图书信息时，修改传过来的所有属性
     *
     * @param data 单个数据
     */
    @Override
    public boolean update(Book data) {
        if (data == null) {
            ExceptionHelper.throw500("数据为空");
        }
        String logMsg;
        logMsg = "修改图书信息";
        logger.info(logMsg);
        // 避免修改到文件id
        data.setBookFileId(null);
        return bookMapper.updateByPrimaryKeySelective(data) > 0;
    }

    @Override
    public List<Book> getDataList(String key, Integer otherId) {
        mExample = createExample();
        BookExample.Criteria c = mExample.createCriteria();
        switch (key) {
            case "fileId":
                // 根据文件id查询书籍信息
                c.andBookFileIdEqualTo(otherId);
            case "libId":
                // 查询某图书馆的书籍列表
                c.andBookLibIdEqualTo(otherId);
                break;
            case "tagId":
                // 根据标签id查询书籍列表
                if (!existBookTag(otherId)) {
                    logger.error("不存在此标签：" + otherId);
                }
                c.andBookTagEqualTo(otherId);
                break;
            default:
                break;
        }
        return bookMapper.selectByExample(mExample);
    }

    /**
     * 验证是否存在书籍id
     *
     * @param tagId
     */
    private boolean existBookTag(Integer tagId) {
        return bookTagMapper.selectByPrimaryKey(tagId) != null;
    }

    @Override
    public List<Book> getDataList(Integer id) {
        mExample = createExample();
        BookExample.Criteria c = mExample.createCriteria();
        c.andBookFileIdEqualTo(id);
        return bookMapper.selectByExample(mExample);
    }

    /**
     * @param key 根据关键词(书名或作者)搜索数据列表
     * @return
     */
    @Override
    public List<Book> getDataList2(String key, String value) {
        switch (key) {
            case "search":
                return searchBooks(value);
            case "userId":
                return getBooksByUserId(value);
            case "libId":
                return getBooksByLibId(value);
            default:
                break;
        }
        return super.getDataList2(key, value);
    }

    private List<Book> getBooksByLibId(String libId) {
        int id = MyUtils.tryParseInt(libId, -1);
        mExample = createExample();
        mExample.createCriteria().andBookLibIdEqualTo(id);
        return bookMapper.selectByExample(mExample);
    }

    private List<Book> getBooksByUserId(String userId) {
        int id = MyUtils.tryParseInt(userId, -1);
        mExample = createExample();
        mExample.createCriteria().andBookOwnerEqualTo(id);
        return bookMapper.selectByExample(mExample);
    }

    private List<Book> searchBooks(String keyword) {
        String condition = "%" + keyword + "%";
        mExample = createExample();
        BookExample.Criteria c1 = mExample.createCriteria();
        // 全匹配
        c1.andBookNameLike(condition);
        BookExample.Criteria c2 = mExample.createCriteria();
        c2.andBookAuthorLike(condition);
        mExample.or(c2);
        return bookMapper.selectByExample(mExample);
    }

    public List<Book> getTop5() {
        return bookMapper.getTop5();
    }

    public List<BookBorRec> myBorrows(Integer userId) {
        return bookBorRecMapper.myBorrows(userId);
    }

    public List<BookBorRec> myBorrowed(Integer userId) {
        return bookBorRecMapper.myBorrowed(userId);
    }

    public boolean updateFileId(Integer bookId, Integer fileId) {
        Book book = new Book();
        book.setBookId(bookId);
        book.setBookFileId(fileId);
        return bookMapper.updateByPrimaryKeySelective(book) > 0;
    }

    /**
     * 修改书籍状态，与借阅记录表同步
     *
     * @param bookId 书籍id
     * @param status 借阅状态
     */
    public void updateStatus(Integer bookId, int status) {
        Book book = new Book();
        book.setBookId(bookId);
        book.setBookStatus(status);
        bookMapper.updateByPrimaryKeySelective(book);
    }

    public List<Book> myCollect(Integer userId) {
        return bookMapper.myCollect(userId);
    }

}
