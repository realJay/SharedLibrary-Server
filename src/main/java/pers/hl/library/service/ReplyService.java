package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.ReplyMapper;
import pers.hl.library.po.Reply;
import pers.hl.library.po.ReplyExample;

import java.util.List;

@Service
public class ReplyService extends BaseService<Reply, ReplyExample> {

    @Autowired
    public ReplyService(ReplyMapper replyMapper) {
        init(replyMapper);
    }


    @Override
    public List<Reply> getDataList(String key, Integer otherId) {
        switch (key) {
            case "cmtId":
                mExample = createExample();
                mExample.createCriteria().andRplCmtIdEqualTo(otherId);
                return dao.selectByExample(mExample);
        }
        return super.getDataList(key, otherId);
    }
}
