package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.CollectMapper;
import pers.hl.library.po.Collect;
import pers.hl.library.po.CollectExample;

import java.util.List;

@Service
public class CollectService extends BaseService<Collect, CollectExample> {

    @Autowired
    CollectMapper collectMapper;

    public CollectService(CollectMapper collectMapper) {
        init(collectMapper);
    }

    @Override
    public List<Collect> getDataList(String key, Integer otherId) {
        mExample = createExample();
        CollectExample.Criteria c = mExample.createCriteria();
        switch (key) {
            case "userId":
                c.andUserIdEqualTo(otherId);
                return dao.selectByExample(mExample);
        }
        return super.getDataList(key, otherId);
    }

    public boolean deleteByBookId(Integer bookId, Integer userId) {
        mExample = createExample();
        mExample.createCriteria().andBookIdEqualTo(bookId).andUserIdEqualTo(userId);
        return dao.deleteByExample(mExample) > 0;
    }

    public List<Collect> getDataByUserBookId(Integer bookId, Integer userId) {
        mExample = createExample();
        mExample.createCriteria().andBookIdEqualTo(bookId).andUserIdEqualTo(userId);
        return dao.selectByExample(mExample);
    }
}
