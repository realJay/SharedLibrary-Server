package pers.hl.library.service;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.FileMapper;
import pers.hl.library.enums.FileType;
import pers.hl.library.po.FileEntity;
import pers.hl.library.po.FileExample;
import pers.hl.library.utils.FileUtils;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService extends BaseService<FileEntity, FileExample> {

    /**
     * 查询关键字：业务id
     */
    private static final String SELECT_KEY_BUSS_ID =  "bussId";

    @Resource
    FileMapper fileMapper;

    @Override
    public FileEntity getDataByPrimaryId(Integer id) {
        return fileMapper.selectByPrimaryKey(id);
    }


    /**
     * 获取最新记录
     * @param fileType 文件类型 对应{@link FileType}
     * @param bussId 业务id 类型对应的表主键id
     * @return
     */
    public FileEntity getNewestByPrimaryId(int fileType, Integer bussId) {
        mExample = createExample();
        FileExample.Criteria c = mExample.createCriteria();
        c.andFileTypeEqualTo(fileType);
        c.andFileBussIdEqualTo(bussId);
        mExample.setOrderByClause("file_create_time desc");
        List<FileEntity> list = fileMapper.selectByExample(mExample);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public boolean addData(FileEntity data) {
        return fileMapper.insert(data) > 0;
    }

    @Override
    public boolean batchAdd(List<FileEntity> list) {
        return false;
    }

    @Override
    public boolean deleteData(Integer id) {
        return false;
    }

    @Override
    public boolean batchDelete(List<Integer> ids) {
        List<Integer> list = new ArrayList<>();
        ids.forEach(id -> {
            if (id <= 0) {
                logger.error("无效文件id,跳过");
                return;
            }
            FileEntity data = getDataByPrimaryId(id);
            if (data == null) {
                logger.error("文件不存在,跳过,id=" + id);
                return;
            }
            String filePath = data.getFileUrl();
            if (StringUtils.isEmpty(filePath)) {
                return;
            }
            FileUtils.deleteFile(new File(filePath));
            list.add(id);
        });
        return fileMapper.batchDelete(list) > 0;
    }

    @Override
    public boolean update(FileEntity data) {
        return false;
    }

    @Override
    public List<FileEntity> getDataList(Integer id) {
        mExample = createExample();
        FileExample.Criteria c = mExample.createCriteria();
        c.andFileBussIdEqualTo(id);
        return fileMapper.selectByExample(mExample);
    }

    @Override
    public List<FileEntity> getDataList(String key, Integer otherId) {
        mExample = createExample();
        FileExample.Criteria c = mExample.createCriteria();
        switch (key) {
            case SELECT_KEY_BUSS_ID:
                c.andFileBussIdEqualTo(otherId);
                break;
            default:
                break;
        }
        return fileMapper.selectByExample(mExample);
    }
}
