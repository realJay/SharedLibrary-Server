package pers.hl.library.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pers.hl.library.common.base.BaseService;
import pers.hl.library.dao.BookShareRecMapper;
import pers.hl.library.po.BookShareRec;
import pers.hl.library.po.BookShareRecExample;

import java.util.List;

@Service
public class BookShareRecordService extends BaseService<BookShareRec, BookShareRecExample> {

    @Autowired
    BookShareRecMapper mapper;

    @Override
    public BookShareRec getDataByPrimaryId(Integer id) {
        return null;
    }

    @Override
    public boolean addData(BookShareRec data) {
        return mapper.insertSelective(data) > 0;
    }

    @Override
    public boolean batchAdd(List<BookShareRec> list) {
        return false;
    }

    @Override
    public boolean deleteData(Integer id) {
        return mapper.deleteByPrimaryKey(id) > 0;
    }

    @Override
    public boolean batchDelete(List<Integer> ids) {
        return mapper.batchDelete(ids) > 0;
    }

    @Override
    public boolean update(BookShareRec data) {
        return mapper.updateByPrimaryKeySelective(data) > 0;
    }

    public List<BookShareRec> myPublish(Integer userId) {
        return mapper.myPublish(userId);
    }

    public List<BookShareRec> mySharing(Integer userId) {
        return mapper.mySharing(userId);
    }
}
