package pers.hl.library.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class User {

    // 普通用户
    public static final int ROLE_COMMON = 1;
    // 管理员
    public static final int ROLE_ADMIN = 2;

    private Integer userId;

    private String userName;

    private Integer userGender;

    private Integer useAge;

    private String userAddr;

    private Integer userCredit;

    private Integer userRole;

    private String userTel;

    private String account;

    @JsonIgnore
    private String password;

    private String token;

    @JsonIgnore
    public boolean isAdmin() {
        return userRole == ROLE_ADMIN;
    }

    private Integer sharingCount;

    private Integer followCount;

    private Integer fansCount;

}