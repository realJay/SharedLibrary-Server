package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FollowRecExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FollowRecExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andFrIdIsNull() {
            addCriterion("fr_id is null");
            return (Criteria) this;
        }

        public Criteria andFrIdIsNotNull() {
            addCriterion("fr_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrIdEqualTo(Integer value) {
            addCriterion("fr_id =", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdNotEqualTo(Integer value) {
            addCriterion("fr_id <>", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdGreaterThan(Integer value) {
            addCriterion("fr_id >", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("fr_id >=", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdLessThan(Integer value) {
            addCriterion("fr_id <", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdLessThanOrEqualTo(Integer value) {
            addCriterion("fr_id <=", value, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdIn(List<Integer> values) {
            addCriterion("fr_id in", values, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdNotIn(List<Integer> values) {
            addCriterion("fr_id not in", values, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdBetween(Integer value1, Integer value2) {
            addCriterion("fr_id between", value1, value2, "frId");
            return (Criteria) this;
        }

        public Criteria andFrIdNotBetween(Integer value1, Integer value2) {
            addCriterion("fr_id not between", value1, value2, "frId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdIsNull() {
            addCriterion("fr_user_id is null");
            return (Criteria) this;
        }

        public Criteria andFrUserIdIsNotNull() {
            addCriterion("fr_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrUserIdEqualTo(Integer value) {
            addCriterion("fr_user_id =", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdNotEqualTo(Integer value) {
            addCriterion("fr_user_id <>", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdGreaterThan(Integer value) {
            addCriterion("fr_user_id >", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("fr_user_id >=", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdLessThan(Integer value) {
            addCriterion("fr_user_id <", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("fr_user_id <=", value, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdIn(List<Integer> values) {
            addCriterion("fr_user_id in", values, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdNotIn(List<Integer> values) {
            addCriterion("fr_user_id not in", values, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdBetween(Integer value1, Integer value2) {
            addCriterion("fr_user_id between", value1, value2, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("fr_user_id not between", value1, value2, "frUserId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdIsNull() {
            addCriterion("fr_follower_id is null");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdIsNotNull() {
            addCriterion("fr_follower_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdEqualTo(Integer value) {
            addCriterion("fr_follower_id =", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdNotEqualTo(Integer value) {
            addCriterion("fr_follower_id <>", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdGreaterThan(Integer value) {
            addCriterion("fr_follower_id >", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("fr_follower_id >=", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdLessThan(Integer value) {
            addCriterion("fr_follower_id <", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdLessThanOrEqualTo(Integer value) {
            addCriterion("fr_follower_id <=", value, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdIn(List<Integer> values) {
            addCriterion("fr_follower_id in", values, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdNotIn(List<Integer> values) {
            addCriterion("fr_follower_id not in", values, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdBetween(Integer value1, Integer value2) {
            addCriterion("fr_follower_id between", value1, value2, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrFollowerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("fr_follower_id not between", value1, value2, "frFollowerId");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeIsNull() {
            addCriterion("fr_create_time is null");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeIsNotNull() {
            addCriterion("fr_create_time is not null");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeEqualTo(Date value) {
            addCriterion("fr_create_time =", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeNotEqualTo(Date value) {
            addCriterion("fr_create_time <>", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeGreaterThan(Date value) {
            addCriterion("fr_create_time >", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("fr_create_time >=", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeLessThan(Date value) {
            addCriterion("fr_create_time <", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("fr_create_time <=", value, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeIn(List<Date> values) {
            addCriterion("fr_create_time in", values, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeNotIn(List<Date> values) {
            addCriterion("fr_create_time not in", values, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeBetween(Date value1, Date value2) {
            addCriterion("fr_create_time between", value1, value2, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("fr_create_time not between", value1, value2, "frCreateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeIsNull() {
            addCriterion("fr_update_time is null");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeIsNotNull() {
            addCriterion("fr_update_time is not null");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeEqualTo(Date value) {
            addCriterion("fr_update_time =", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeNotEqualTo(Date value) {
            addCriterion("fr_update_time <>", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeGreaterThan(Date value) {
            addCriterion("fr_update_time >", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("fr_update_time >=", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeLessThan(Date value) {
            addCriterion("fr_update_time <", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("fr_update_time <=", value, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeIn(List<Date> values) {
            addCriterion("fr_update_time in", values, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeNotIn(List<Date> values) {
            addCriterion("fr_update_time not in", values, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("fr_update_time between", value1, value2, "frUpdateTime");
            return (Criteria) this;
        }

        public Criteria andFrUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("fr_update_time not between", value1, value2, "frUpdateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}