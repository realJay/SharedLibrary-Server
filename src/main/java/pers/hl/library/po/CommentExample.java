package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CommentExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CommentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCmtIdIsNull() {
            addCriterion("cmt_id is null");
            return (Criteria) this;
        }

        public Criteria andCmtIdIsNotNull() {
            addCriterion("cmt_id is not null");
            return (Criteria) this;
        }

        public Criteria andCmtIdEqualTo(Integer value) {
            addCriterion("cmt_id =", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdNotEqualTo(Integer value) {
            addCriterion("cmt_id <>", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdGreaterThan(Integer value) {
            addCriterion("cmt_id >", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("cmt_id >=", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdLessThan(Integer value) {
            addCriterion("cmt_id <", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdLessThanOrEqualTo(Integer value) {
            addCriterion("cmt_id <=", value, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdIn(List<Integer> values) {
            addCriterion("cmt_id in", values, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdNotIn(List<Integer> values) {
            addCriterion("cmt_id not in", values, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdBetween(Integer value1, Integer value2) {
            addCriterion("cmt_id between", value1, value2, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtIdNotBetween(Integer value1, Integer value2) {
            addCriterion("cmt_id not between", value1, value2, "cmtId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdIsNull() {
            addCriterion("cmt_user_id is null");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdIsNotNull() {
            addCriterion("cmt_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdEqualTo(Integer value) {
            addCriterion("cmt_user_id =", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdNotEqualTo(Integer value) {
            addCriterion("cmt_user_id <>", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdGreaterThan(Integer value) {
            addCriterion("cmt_user_id >", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("cmt_user_id >=", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdLessThan(Integer value) {
            addCriterion("cmt_user_id <", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("cmt_user_id <=", value, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdIn(List<Integer> values) {
            addCriterion("cmt_user_id in", values, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdNotIn(List<Integer> values) {
            addCriterion("cmt_user_id not in", values, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdBetween(Integer value1, Integer value2) {
            addCriterion("cmt_user_id between", value1, value2, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("cmt_user_id not between", value1, value2, "cmtUserId");
            return (Criteria) this;
        }

        public Criteria andCmtInfoIsNull() {
            addCriterion("cmt_info is null");
            return (Criteria) this;
        }

        public Criteria andCmtInfoIsNotNull() {
            addCriterion("cmt_info is not null");
            return (Criteria) this;
        }

        public Criteria andCmtInfoEqualTo(String value) {
            addCriterion("cmt_info =", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoNotEqualTo(String value) {
            addCriterion("cmt_info <>", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoGreaterThan(String value) {
            addCriterion("cmt_info >", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoGreaterThanOrEqualTo(String value) {
            addCriterion("cmt_info >=", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoLessThan(String value) {
            addCriterion("cmt_info <", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoLessThanOrEqualTo(String value) {
            addCriterion("cmt_info <=", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoLike(String value) {
            addCriterion("cmt_info like", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoNotLike(String value) {
            addCriterion("cmt_info not like", value, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoIn(List<String> values) {
            addCriterion("cmt_info in", values, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoNotIn(List<String> values) {
            addCriterion("cmt_info not in", values, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoBetween(String value1, String value2) {
            addCriterion("cmt_info between", value1, value2, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtInfoNotBetween(String value1, String value2) {
            addCriterion("cmt_info not between", value1, value2, "cmtInfo");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdIsNull() {
            addCriterion("cmt_book_id is null");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdIsNotNull() {
            addCriterion("cmt_book_id is not null");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdEqualTo(Integer value) {
            addCriterion("cmt_book_id =", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdNotEqualTo(Integer value) {
            addCriterion("cmt_book_id <>", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdGreaterThan(Integer value) {
            addCriterion("cmt_book_id >", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("cmt_book_id >=", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdLessThan(Integer value) {
            addCriterion("cmt_book_id <", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdLessThanOrEqualTo(Integer value) {
            addCriterion("cmt_book_id <=", value, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdIn(List<Integer> values) {
            addCriterion("cmt_book_id in", values, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdNotIn(List<Integer> values) {
            addCriterion("cmt_book_id not in", values, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdBetween(Integer value1, Integer value2) {
            addCriterion("cmt_book_id between", value1, value2, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtBookIdNotBetween(Integer value1, Integer value2) {
            addCriterion("cmt_book_id not between", value1, value2, "cmtBookId");
            return (Criteria) this;
        }

        public Criteria andCmtTimeIsNull() {
            addCriterion("cmt_time is null");
            return (Criteria) this;
        }

        public Criteria andCmtTimeIsNotNull() {
            addCriterion("cmt_time is not null");
            return (Criteria) this;
        }

        public Criteria andCmtTimeEqualTo(Date value) {
            addCriterion("cmt_time =", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeNotEqualTo(Date value) {
            addCriterion("cmt_time <>", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeGreaterThan(Date value) {
            addCriterion("cmt_time >", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cmt_time >=", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeLessThan(Date value) {
            addCriterion("cmt_time <", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeLessThanOrEqualTo(Date value) {
            addCriterion("cmt_time <=", value, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeIn(List<Date> values) {
            addCriterion("cmt_time in", values, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeNotIn(List<Date> values) {
            addCriterion("cmt_time not in", values, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeBetween(Date value1, Date value2) {
            addCriterion("cmt_time between", value1, value2, "cmtTime");
            return (Criteria) this;
        }

        public Criteria andCmtTimeNotBetween(Date value1, Date value2) {
            addCriterion("cmt_time not between", value1, value2, "cmtTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}