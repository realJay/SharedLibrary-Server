package pers.hl.library.po;

import pers.hl.library.common.base.IExample;

import java.util.ArrayList;
import java.util.List;

public class BookExample implements IExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BookExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andBookIdIsNull() {
            addCriterion("book_id is null");
            return (Criteria) this;
        }

        public Criteria andBookIdIsNotNull() {
            addCriterion("book_id is not null");
            return (Criteria) this;
        }

        public Criteria andBookIdEqualTo(Integer value) {
            addCriterion("book_id =", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotEqualTo(Integer value) {
            addCriterion("book_id <>", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdGreaterThan(Integer value) {
            addCriterion("book_id >", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_id >=", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdLessThan(Integer value) {
            addCriterion("book_id <", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdLessThanOrEqualTo(Integer value) {
            addCriterion("book_id <=", value, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdIn(List<Integer> values) {
            addCriterion("book_id in", values, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotIn(List<Integer> values) {
            addCriterion("book_id not in", values, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdBetween(Integer value1, Integer value2) {
            addCriterion("book_id between", value1, value2, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookIdNotBetween(Integer value1, Integer value2) {
            addCriterion("book_id not between", value1, value2, "bookId");
            return (Criteria) this;
        }

        public Criteria andBookNameIsNull() {
            addCriterion("book_name is null");
            return (Criteria) this;
        }

        public Criteria andBookNameIsNotNull() {
            addCriterion("book_name is not null");
            return (Criteria) this;
        }

        public Criteria andBookNameEqualTo(String value) {
            addCriterion("book_name =", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameNotEqualTo(String value) {
            addCriterion("book_name <>", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameGreaterThan(String value) {
            addCriterion("book_name >", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameGreaterThanOrEqualTo(String value) {
            addCriterion("book_name >=", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameLessThan(String value) {
            addCriterion("book_name <", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameLessThanOrEqualTo(String value) {
            addCriterion("book_name <=", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameLike(String value) {
            addCriterion("book_name like", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameNotLike(String value) {
            addCriterion("book_name not like", value, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameIn(List<String> values) {
            addCriterion("book_name in", values, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameNotIn(List<String> values) {
            addCriterion("book_name not in", values, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameBetween(String value1, String value2) {
            addCriterion("book_name between", value1, value2, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookNameNotBetween(String value1, String value2) {
            addCriterion("book_name not between", value1, value2, "bookName");
            return (Criteria) this;
        }

        public Criteria andBookIsbnIsNull() {
            addCriterion("book_isbn is null");
            return (Criteria) this;
        }

        public Criteria andBookIsbnIsNotNull() {
            addCriterion("book_isbn is not null");
            return (Criteria) this;
        }

        public Criteria andBookIsbnEqualTo(Long value) {
            addCriterion("book_isbn =", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnNotEqualTo(Long value) {
            addCriterion("book_isbn <>", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnGreaterThan(Long value) {
            addCriterion("book_isbn >", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnGreaterThanOrEqualTo(Long value) {
            addCriterion("book_isbn >=", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnLessThan(Long value) {
            addCriterion("book_isbn <", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnLessThanOrEqualTo(Long value) {
            addCriterion("book_isbn <=", value, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnIn(List<Long> values) {
            addCriterion("book_isbn in", values, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnNotIn(List<Long> values) {
            addCriterion("book_isbn not in", values, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnBetween(Long value1, Long value2) {
            addCriterion("book_isbn between", value1, value2, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookIsbnNotBetween(Long value1, Long value2) {
            addCriterion("book_isbn not between", value1, value2, "bookIsbn");
            return (Criteria) this;
        }

        public Criteria andBookOwnerIsNull() {
            addCriterion("book_owner is null");
            return (Criteria) this;
        }

        public Criteria andBookOwnerIsNotNull() {
            addCriterion("book_owner is not null");
            return (Criteria) this;
        }

        public Criteria andBookOwnerEqualTo(Integer value) {
            addCriterion("book_owner =", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerNotEqualTo(Integer value) {
            addCriterion("book_owner <>", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerGreaterThan(Integer value) {
            addCriterion("book_owner >", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_owner >=", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerLessThan(Integer value) {
            addCriterion("book_owner <", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerLessThanOrEqualTo(Integer value) {
            addCriterion("book_owner <=", value, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerIn(List<Integer> values) {
            addCriterion("book_owner in", values, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerNotIn(List<Integer> values) {
            addCriterion("book_owner not in", values, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerBetween(Integer value1, Integer value2) {
            addCriterion("book_owner between", value1, value2, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookOwnerNotBetween(Integer value1, Integer value2) {
            addCriterion("book_owner not between", value1, value2, "bookOwner");
            return (Criteria) this;
        }

        public Criteria andBookFileIdIsNull() {
            addCriterion("book_file_id is null");
            return (Criteria) this;
        }

        public Criteria andBookFileIdIsNotNull() {
            addCriterion("book_file_id is not null");
            return (Criteria) this;
        }

        public Criteria andBookFileIdEqualTo(Integer value) {
            addCriterion("book_file_id =", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdNotEqualTo(Integer value) {
            addCriterion("book_file_id <>", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdGreaterThan(Integer value) {
            addCriterion("book_file_id >", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_file_id >=", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdLessThan(Integer value) {
            addCriterion("book_file_id <", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdLessThanOrEqualTo(Integer value) {
            addCriterion("book_file_id <=", value, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdIn(List<Integer> values) {
            addCriterion("book_file_id in", values, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdNotIn(List<Integer> values) {
            addCriterion("book_file_id not in", values, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdBetween(Integer value1, Integer value2) {
            addCriterion("book_file_id between", value1, value2, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookFileIdNotBetween(Integer value1, Integer value2) {
            addCriterion("book_file_id not between", value1, value2, "bookFileId");
            return (Criteria) this;
        }

        public Criteria andBookStatusIsNull() {
            addCriterion("book_status is null");
            return (Criteria) this;
        }

        public Criteria andBookStatusIsNotNull() {
            addCriterion("book_status is not null");
            return (Criteria) this;
        }

        public Criteria andBookStatusEqualTo(Integer value) {
            addCriterion("book_status =", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusNotEqualTo(Integer value) {
            addCriterion("book_status <>", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusGreaterThan(Integer value) {
            addCriterion("book_status >", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_status >=", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusLessThan(Integer value) {
            addCriterion("book_status <", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusLessThanOrEqualTo(Integer value) {
            addCriterion("book_status <=", value, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusIn(List<Integer> values) {
            addCriterion("book_status in", values, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusNotIn(List<Integer> values) {
            addCriterion("book_status not in", values, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusBetween(Integer value1, Integer value2) {
            addCriterion("book_status between", value1, value2, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("book_status not between", value1, value2, "bookStatus");
            return (Criteria) this;
        }

        public Criteria andBookDescIsNull() {
            addCriterion("book_desc is null");
            return (Criteria) this;
        }

        public Criteria andBookDescIsNotNull() {
            addCriterion("book_desc is not null");
            return (Criteria) this;
        }

        public Criteria andBookDescEqualTo(String value) {
            addCriterion("book_desc =", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescNotEqualTo(String value) {
            addCriterion("book_desc <>", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescGreaterThan(String value) {
            addCriterion("book_desc >", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescGreaterThanOrEqualTo(String value) {
            addCriterion("book_desc >=", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescLessThan(String value) {
            addCriterion("book_desc <", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescLessThanOrEqualTo(String value) {
            addCriterion("book_desc <=", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescLike(String value) {
            addCriterion("book_desc like", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescNotLike(String value) {
            addCriterion("book_desc not like", value, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescIn(List<String> values) {
            addCriterion("book_desc in", values, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescNotIn(List<String> values) {
            addCriterion("book_desc not in", values, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescBetween(String value1, String value2) {
            addCriterion("book_desc between", value1, value2, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookDescNotBetween(String value1, String value2) {
            addCriterion("book_desc not between", value1, value2, "bookDesc");
            return (Criteria) this;
        }

        public Criteria andBookAuthorIsNull() {
            addCriterion("book_author is null");
            return (Criteria) this;
        }

        public Criteria andBookAuthorIsNotNull() {
            addCriterion("book_author is not null");
            return (Criteria) this;
        }

        public Criteria andBookAuthorEqualTo(String value) {
            addCriterion("book_author =", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorNotEqualTo(String value) {
            addCriterion("book_author <>", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorGreaterThan(String value) {
            addCriterion("book_author >", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorGreaterThanOrEqualTo(String value) {
            addCriterion("book_author >=", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorLessThan(String value) {
            addCriterion("book_author <", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorLessThanOrEqualTo(String value) {
            addCriterion("book_author <=", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorLike(String value) {
            addCriterion("book_author like", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorNotLike(String value) {
            addCriterion("book_author not like", value, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorIn(List<String> values) {
            addCriterion("book_author in", values, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorNotIn(List<String> values) {
            addCriterion("book_author not in", values, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorBetween(String value1, String value2) {
            addCriterion("book_author between", value1, value2, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookAuthorNotBetween(String value1, String value2) {
            addCriterion("book_author not between", value1, value2, "bookAuthor");
            return (Criteria) this;
        }

        public Criteria andBookTagIsNull() {
            addCriterion("book_tag is null");
            return (Criteria) this;
        }

        public Criteria andBookTagIsNotNull() {
            addCriterion("book_tag is not null");
            return (Criteria) this;
        }

        public Criteria andBookTagEqualTo(Integer value) {
            addCriterion("book_tag =", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagNotEqualTo(Integer value) {
            addCriterion("book_tag <>", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagGreaterThan(Integer value) {
            addCriterion("book_tag >", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_tag >=", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagLessThan(Integer value) {
            addCriterion("book_tag <", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagLessThanOrEqualTo(Integer value) {
            addCriterion("book_tag <=", value, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagIn(List<Integer> values) {
            addCriterion("book_tag in", values, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagNotIn(List<Integer> values) {
            addCriterion("book_tag not in", values, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagBetween(Integer value1, Integer value2) {
            addCriterion("book_tag between", value1, value2, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookTagNotBetween(Integer value1, Integer value2) {
            addCriterion("book_tag not between", value1, value2, "bookTag");
            return (Criteria) this;
        }

        public Criteria andBookLibIdIsNull() {
            addCriterion("book_lib_id is null");
            return (Criteria) this;
        }

        public Criteria andBookLibIdIsNotNull() {
            addCriterion("book_lib_id is not null");
            return (Criteria) this;
        }

        public Criteria andBookLibIdEqualTo(Integer value) {
            addCriterion("book_lib_id =", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdNotEqualTo(Integer value) {
            addCriterion("book_lib_id <>", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdGreaterThan(Integer value) {
            addCriterion("book_lib_id >", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("book_lib_id >=", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdLessThan(Integer value) {
            addCriterion("book_lib_id <", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdLessThanOrEqualTo(Integer value) {
            addCriterion("book_lib_id <=", value, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdIn(List<Integer> values) {
            addCriterion("book_lib_id in", values, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdNotIn(List<Integer> values) {
            addCriterion("book_lib_id not in", values, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdBetween(Integer value1, Integer value2) {
            addCriterion("book_lib_id between", value1, value2, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookLibIdNotBetween(Integer value1, Integer value2) {
            addCriterion("book_lib_id not between", value1, value2, "bookLibId");
            return (Criteria) this;
        }

        public Criteria andBookImgIsNull() {
            addCriterion("book_img is null");
            return (Criteria) this;
        }

        public Criteria andBookImgIsNotNull() {
            addCriterion("book_img is not null");
            return (Criteria) this;
        }

        public Criteria andBookImgEqualTo(String value) {
            addCriterion("book_img =", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgNotEqualTo(String value) {
            addCriterion("book_img <>", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgGreaterThan(String value) {
            addCriterion("book_img >", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgGreaterThanOrEqualTo(String value) {
            addCriterion("book_img >=", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgLessThan(String value) {
            addCriterion("book_img <", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgLessThanOrEqualTo(String value) {
            addCriterion("book_img <=", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgLike(String value) {
            addCriterion("book_img like", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgNotLike(String value) {
            addCriterion("book_img not like", value, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgIn(List<String> values) {
            addCriterion("book_img in", values, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgNotIn(List<String> values) {
            addCriterion("book_img not in", values, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgBetween(String value1, String value2) {
            addCriterion("book_img between", value1, value2, "bookImg");
            return (Criteria) this;
        }

        public Criteria andBookImgNotBetween(String value1, String value2) {
            addCriterion("book_img not between", value1, value2, "bookImg");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}