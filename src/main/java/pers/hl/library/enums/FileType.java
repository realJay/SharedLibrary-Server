package pers.hl.library.enums;

public enum FileType {

    BOOK_IMG(1), BOOK_QRCODE(2), USER_AVATAR(3), USER_QR_CODE(4);

    private final int type;

    FileType(int type) {
        this.type = type;
    }

    public static FileType parse(int type) {
        switch (type) {
            case 1:
                return BOOK_IMG;
            case 2:
                return BOOK_QRCODE;
            case 3:
                return USER_AVATAR;
            case 4:
                return USER_QR_CODE;
        }
        return null;
    }

    @Override
    public String toString() {
        return String.valueOf(type);
    }

    public static int value(FileType enumType) {
        switch (enumType) {
            case BOOK_IMG:
                return 1;
            case BOOK_QRCODE:
                return 2;
            case USER_AVATAR:
                return 3;
            case USER_QR_CODE:
                return 4;
            default:
                return 0;
        }
    }

    public int value() {
        return type;
    }

    public static boolean exist(Integer type) {
        if (type == null) {
            return false;
        }
        FileType fileType = parse(type);
        return fileType != null;
    }
}
