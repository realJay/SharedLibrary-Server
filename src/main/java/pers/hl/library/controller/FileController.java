package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pers.hl.library.common.Const;
import pers.hl.library.common.Response;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.enums.FileType;
import pers.hl.library.po.FileEntity;
import pers.hl.library.service.FileService;
import pers.hl.library.utils.ExceptionHelper;
import pers.hl.library.utils.FileUtils;
import pers.hl.library.utils.MyUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * 文件相关控制器
 * <p>
 * 用户头像也可以有多张，获取的时候以最新的为准
 */
@RestController
@RequestMapping("file")
public class FileController extends LibBaseController<FileEntity> {

    @Autowired
    FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        init(fileService);
    }

    /**
     * 文件上传
     *
     * @param file       文件
     * @param fileBussId 文件业务id，比如书籍id.用户id等
     * @param fileType   文件类型 1：书籍照片 3：用户头像
     * @return 响应体
     */
    @PostMapping("/upload")
    public Response upload(@RequestParam("file") MultipartFile file, @RequestParam("fileBussId") Integer fileBussId, @RequestParam("fileType") Integer fileType) throws Exception {
        if (file.isEmpty()) {
            return Response.fail_400("文件【file】为空");
        }
        if (StringUtils.isEmpty(fileBussId)) {
            return Response.fail_400("文件类型【fileType】不能为空");
        }
        if (StringUtils.isEmpty(fileType)) {
            return Response.fail_400("文件类型【fileType】不能为空");
        }
        // 获取文件名
        String fileName = file.getOriginalFilename();
        if (StringUtils.isEmpty(fileName)) {
            return Response.fail_400("文件名不能为空");
        }
        FileType type = FileType.parse(fileType);
        if (type == null) {
            return Response.fail_400("文件类型【fileType】不存在");
        }
        fileName = MyUtils.getPureUUID() + System.currentTimeMillis() + fileName.substring(fileName.lastIndexOf("."));
        logger.info("上传的文件名为：" + fileName);

        //设置文件存储路径
        String filePath = Const.FilePath.FILE_PATH;
        switch (fileType) {
            case 1:
                filePath = Const.FilePath.FILE_BOOK_IMG_PATH;
                break;
            case 3:
                filePath = Const.FilePath.FILE_AVATAR_PATH;
                break;
        }
        File dest = new File(filePath, fileName);
        // 向服务器写入文件
        FileUtils.writeFile(file, dest);
        // 创建文件表对应实体，并写入数据库
        FileEntity data = new FileEntity();
        data.setFileType(fileType);
        data.setFileBussId(fileBussId);
        data.setFileUrl(dest.getAbsolutePath());
        Date curDate = new Date();
        data.setFileCreateTime(curDate);
        data.setFileUpdateTime(curDate);
        if (fileService.addData(data)) {
            return Response.ok("上传成功", data);
        }
        return Response.fail_500("上传失败");
    }

    /**
     * 根据文件id获取(下载)文件
     *
     * @param request  请求
     * @param response 响应
     * @param fileId   文件id
     * @throws Exception 各种异常，有全局捕获 see {@link pers.hl.library.common.GlobalDefaultExceptionHandler }
     */
    @GetMapping("download/{fileId}")
    public void download(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer fileId) throws Exception {
        if (fileId <= 0) {
            throw new IllegalArgumentException("文件id不能为空");
        }
        FileEntity fileEntity = fileService.getDataByPrimaryId(fileId);
        if (null == fileEntity) {
            ExceptionHelper.throw404("未找到相关文件");
        }
        String realPath = fileEntity.getFileUrl();
        try {
            FileUtils.readFile(request, response, realPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据文件类型和业务id获取文件(最新)
     * @param fileType
     * @param bussId
     * @throws IOException
     */
    @GetMapping("download/{fileType}/{bussId}")
    public void downloadByTypeBuss(HttpServletRequest request, HttpServletResponse response, @PathVariable Integer fileType, @PathVariable Integer bussId) throws IOException {
        if (!FileType.exist(fileType)) {
            logger.error("不存在此类型的文件");
            ExceptionHelper.throw400("不存在此类型的文件");
        }
        if (bussId <= 0) {
            throw new IllegalArgumentException("业务id不能为空");
        }
        if (fileType <= 0) {
            throw new IllegalArgumentException("文件类型不能为空");
        }
        FileEntity fileEntity = fileService.getNewestByPrimaryId(fileType, bussId);
        if (null == fileEntity) {
            ExceptionHelper.throw404("未找到相关文件");
        }
        String realPath = fileEntity.getFileUrl();
        try {
            FileUtils.readFile(request, response, realPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected String getBussName() {
        return "文件";
    }

}
