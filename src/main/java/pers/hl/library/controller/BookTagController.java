package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.Response;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.BookTag;
import pers.hl.library.service.BookTagService;
import pers.hl.library.utils.ExceptionHelper;

import java.util.List;

@RestController
@RequestMapping("bookTag")
public class BookTagController extends LibBaseController<BookTag> {

    @Autowired
    BookTagService bookTagService;

    @Autowired
    public BookTagController(BookTagService bookTagService) {
        init(bookTagService);
    }

    @Override
    protected String getBussName() {
        return "书籍标签";
    }

    @GetMapping("list/all")
    public Response getAllTags() {
        return Response.ok(bookTagService.getDataList(null));
    }

    @Override
    protected void beforeAdd(BookTag data) throws Exception {
        // 查找是否存在同名的
        List<BookTag> dbData = bookTagService.getTagByName(data.getName());
        if (dbData != null && dbData.size() > 0) {
            ExceptionHelper.throw400("该标签名已存在");
        }
    }
}
