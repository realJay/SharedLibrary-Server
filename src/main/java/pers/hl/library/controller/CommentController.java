package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.Comment;
import pers.hl.library.service.CommentService;

import java.util.Date;

@RestController
@RequestMapping("comment")
public class CommentController extends LibBaseController<Comment> {

    @Autowired
    private CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        init(commentService);
    }

    @Override
    protected String getBussName() {
        return "评论";
    }

    @Override
    protected void beforeAdd(Comment data) {
        data.setCmtUserId(getCurrentUser().getUserId());
        data.setCmtTime(new Date());
    }

    @Override
    protected void beforeUpdate(Comment data) throws Exception {
        data.setCmtTime(new Date());
    }
}
