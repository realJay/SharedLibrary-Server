package pers.hl.library.controller;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.Response;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.FollowRec;
import pers.hl.library.service.FollowService;
import pers.hl.library.utils.ExceptionHelper;

import java.util.Date;

@RestController
@RequestMapping("follow")
public class FollowController extends LibBaseController<FollowRec> {

    @Autowired
    FollowService followService;

    @Autowired
    public FollowController(FollowService followService) {
        init(followService);
    }

    @Override
    protected String getBussName() {
        return "关注";
    }

    @GetMapping("isFollow")
    public Response isFollow(@Param("followerId") int followerId, @Param("userId") int userId) {
        if (followerId < 0 || userId < 0) {
            ExceptionHelper.throw400("参数错误");
        }
        return Response.ok(followService.isFollow(followerId, userId));
    }

    @DeleteMapping("unfollow")
    public Response unfollow(@Param("followerId") int followerId, @Param("userId") int userId) {
        if (followerId < 0 || userId < 0) {
            ExceptionHelper.throw400("参数错误");
        }
        if (followService.unFollow(followerId, userId)) {
            return Response.ok("取消关注成功");
        }
        return Response.fail_500("取消关注失败");
    }

    @Override
    protected void beforeAdd(FollowRec data) throws Exception {
        super.beforeAdd(data);
        data.setFrCreateTime(new Date());
        data.setFrUpdateTime(new Date());
    }

    @Override
    protected void beforeUpdate(FollowRec data) throws Exception {
        data.setFrUpdateTime(new Date());
    }
}
