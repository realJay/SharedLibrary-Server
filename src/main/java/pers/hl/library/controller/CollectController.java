package pers.hl.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pers.hl.library.common.base.LibBaseController;
import pers.hl.library.po.Collect;
import pers.hl.library.service.CollectService;

import java.util.Date;

@RestController
@RequestMapping("collect")
public class CollectController extends LibBaseController<Collect> {

    @Autowired
    CollectService collectService;

    @Override
    protected String getBussName() {
        return "收藏";
    }

    public CollectController(CollectService collectService) {
        init(collectService);
    }

    @Override
    protected void beforeAdd(Collect data) throws Exception {
        data.setUserId(getCurrentUser().getUserId());
        data.setCreateTime(new Date());
    }
}
