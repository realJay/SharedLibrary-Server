package pers.hl.library.common;

public class CustomException extends ServiceException {

    public static void throwNew(String errorCode, String message) {
        throw new CustomException(errorCode, message);
    }

    public CustomException(String errorCode, String message) {
        super(errorCode, message);
    }
}
