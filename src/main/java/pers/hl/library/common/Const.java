package pers.hl.library.common;

import java.text.SimpleDateFormat;

public interface Const {

    long TOKEN_EXPIRES_HOUR = 2 * 60 * 60;

    String CURRENT_USER_ID = "current_user_id";

    int BYTE = 1;
    int KB = 1024;
    int MB = 1048576;
    int GB = 1073741824;

    /**
     * 默认距离
     */
    int DEFAULT_DISTANCE = 1000;

    interface Auth {
        String HEADER = "Authorization";
        String CLAIM_KEY_USER_ID = "userId";
    }


    interface HttpStatusCode {

        /**
         * 成功
         */
        String HttpStatus_200 = "200";
        /**
         * 参数错误
         */
        String HttpStatus_400 = "400";
        /**
         * 未通过身份认证
         */
        String HttpStatus_401 = "401";
        /**
         * 没有访问对应资源的权限
         */
        String HttpStatus_403 = "403";
        /**
         * 未找到资源
         */
        String HttpStatus_404 = "404";
        /**
         * 服务端错误
         */
        String HttpStatus_500 = "500";
    }

    interface DateFormat {

        SimpleDateFormat WITH_HMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat WITHOUT_HMS = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat WITHOUT_HMS_00 = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        SimpleDateFormat HHMM = new SimpleDateFormat("HH:mm");
        SimpleDateFormat HMM = new SimpleDateFormat("H:mm");
        SimpleDateFormat MMDDHHmm = new SimpleDateFormat("MM-dd HH:mm");
        SimpleDateFormat CN_M_D = new SimpleDateFormat("M月d日");
        SimpleDateFormat CN_MM_DD = new SimpleDateFormat("MM月dd日");
        SimpleDateFormat CN_MD_H_m = new SimpleDateFormat("M月d日 H时m分");
        SimpleDateFormat CN_WITHOUT_HMS = new SimpleDateFormat("yyyy年MM月dd日");
    }

    interface DEFAULT_ID {
        int DATA_OPERATE_FAIL_ID = 0;
    }

    interface INTERVAL {
        int TODAY = 1;
        int WEEK = 2;
        int MONTH = 3;
    }

    interface PageParams {

        // 默认页码
        int DEFAULT_PAGE_NUM = 1;
        // 默认每页数量
        int DEFAULT_PAGE_SIZE = 20;

    }

    /**
     * 部署地址,可萌是前缀
     */
    interface Url {
        String URL_ERROR = "/error";
        String URL_TEST = "/test";
        String URL_LOGIN = "/login";
        String URL_USER = "/user";
        String URL_FILE = "/file";
        String URL_SIGN_UP = "/sign_up";
    }

    interface Keys {
        String KEY_USER = "KEY_USER";
    }

    interface RequestKey {
        String KEY_ALL = "all";
    }

    /**
     * 文件类型
     */
    interface FileType {
        // 书籍照片
        String TYPE_BOOK_IMG = "type_book_img";
        // 书籍二维码照片
        String TYPE_BOOK_QRCODE = "type_book_qrcode";
        // 用户头像
        String TYPE_USER_AVATAR = "type_user_avatar";
    }

    /**
     * 二维码地址
     */
    interface FilePath {
        // 文件存放路径
        String FILE_PATH = "D:\\share_library\\file";
        // 二维码存放路径
        String QRCODE_PATH = "D:\\share_library\\file\\qrcode";
        // 书籍图片存放路径
        String FILE_BOOK_IMG_PATH = "D:\\share_library\\file\\book";
        // 用户头像存放路径
        String FILE_AVATAR_PATH = "D:\\share_library\\file\\avatar";

        // 默认用户二维码中间图像
        String DEFAULT_AVATAR_PATH = "src/main/resources/static/image/qrcode/ic_avatar_default.jpg";
        // 默认书籍二维码中间图像
        String DEFAULT_BOOK_QR_IMG_PATH = "src/main/resources/static/image/qrcode/ic_launcher.png";
    }

}
