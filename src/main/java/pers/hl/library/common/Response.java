package pers.hl.library.common;

import com.fasterxml.jackson.databind.JsonNode;
import pers.hl.library.utils.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自定义返回数据结构类
 */
public class Response {

    private static final String DEFAULT_SERVER_ERROR = "服务端未知错误";

    // 定义jackson对象
    private static final CustomMapper MAPPER = new CustomMapper();

    // 响应业务状态
    private String code;

    // 响应消息
    private String msg;

    // 响应中的数据
    private Object data;

    public static Response build(String code, String msg, Object data) {
        LogUtils.e(msg);
        return new Response(code, msg, data);
    }

    public static Response ok(Object data) {
        return new Response(data);
    }

    public static Response ok() {
        return new Response(null);
    }

    public static Response ok(Map<Object, Object> map) {
        return new Response(Const.HttpStatusCode.HttpStatus_200, "查询列表成功", map);
    }

    public static Response ok(String msg, Object object) {
        return new Response(Const.HttpStatusCode.HttpStatus_200, msg, object);
    }

    public static Response ok(String msg) {
        return new Response(Const.HttpStatusCode.HttpStatus_200, msg, null);
    }

    public Response() {

    }

    public static Response build(String code, String msg) {
        LogUtils.e(msg);
        return new Response(code, msg, new Object());
    }

    public Response(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public Response(Object data) {
        this.code = Const.HttpStatusCode.HttpStatus_200;
        this.msg = "OK";
        this.data = data;
    }

    public static Response fail_400() {
        return fail_400("参数错误");
    }

    public static Response fail_400(String msg) {
        return build(Const.HttpStatusCode.HttpStatus_400, msg);
    }

    public static Response fail_401(String msg) {
        return build(Const.HttpStatusCode.HttpStatus_401, msg, new Object());
    }

    public static Response fail_401(String msg, Map<Object, Object> map) {
        return build(Const.HttpStatusCode.HttpStatus_401, msg, map);
    }

    public static Response fail_500(String msg) {
        return build(Const.HttpStatusCode.HttpStatus_500, msg);
    }

    public static Response fail_500() {
        return build(Const.HttpStatusCode.HttpStatus_500, DEFAULT_SERVER_ERROR);
    }

    public static Response fail_obj_500(String key) {
        Map<Object, Object> map = new HashMap<>();
        map.put(key, new Object());
        return build(Const.HttpStatusCode.HttpStatus_500, DEFAULT_SERVER_ERROR, map);
    }

    public static Response fail_array_500(String key) {
        Map<Object, Object> map = new HashMap<>();
        map.put(key, new ArrayList<>());
        return build(Const.HttpStatusCode.HttpStatus_500, DEFAULT_SERVER_ERROR, map);
    }

    public static Response fail_500(Map<Object, Object> map) {
        return build(Const.HttpStatusCode.HttpStatus_500, DEFAULT_SERVER_ERROR, map);
    }

    public static Response fail(String code, String message) {
        return build(code, message);
    }

//    public Boolean isOK() {
//        return this.status == 200;
//    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    /**
     * 将json结果集转化为Result对象
     *
     * @param jsonData json数据
     * @param clazz TaotaoResult中的object类型
     * @return
     */
    public static Response formatToPojo(String jsonData, Class<?> clazz) {
        try {
            if (clazz == null) {
                return MAPPER.readValue(jsonData, Response.class);
            }
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (clazz != null) {
                if (data.isObject()) {
                    obj = MAPPER.readValue(data.traverse(), clazz);
                } else if (data.isTextual()) {
                    obj = MAPPER.readValue(data.asText(), clazz);
                }
            }
            return build(jsonNode.get("status").toString(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 没有object对象的转化
     *
     * @param json
     * @return
     */
    public static Response format(String json) {
        try {
            return MAPPER.readValue(json, Response.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Object是集合转化
     *
     * @param jsonData json数据
     * @param clazz 集合中的类型
     * @return
     */
    public static Response formatToList(String jsonData, Class<?> clazz) {
        try {
            JsonNode jsonNode = MAPPER.readTree(jsonData);
            JsonNode data = jsonNode.get("data");
            Object obj = null;
            if (data.isArray() && data.size() > 0) {
                obj = MAPPER.readValue(data.traverse(),
                        MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
            }
            return build(jsonNode.get("code").toString(), jsonNode.get("msg").asText(), obj);
        } catch (Exception e) {
            return null;
        }
    }

}
