package pers.hl.library;

import org.junit.jupiter.api.Test;
import pers.hl.library.auth.JwtUtils;

import java.util.HashMap;
import java.util.Map;

public class JwtTest {

    private static final String CLAIM_KEY_USER_ID = "userId";

    @Test
    public void tokenTest() throws Exception {
        // 生成token
        Map<String, Object> map = new HashMap<>();
        map.put(CLAIM_KEY_USER_ID, "testId");
        map.put("userName", "RookieJay");
        String token = JwtUtils.createToken(map);
        System.out.println("生成token=" + token);

        // 解析token
        Map<String, Object> parseResult = JwtUtils.parseToken(token);
        System.out.println("解析结果=" + map);
    }
}
